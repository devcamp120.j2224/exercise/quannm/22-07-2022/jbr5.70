package com.devcamp.jbr570.jbr570.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr570.jbr570.model.Animal;
import com.devcamp.jbr570.jbr570.model.Cat;
import com.devcamp.jbr570.jbr570.model.Dog;
import com.devcamp.jbr570.jbr570.service.AnimalService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class AnimalController {

    @GetMapping("/cats")
    public ArrayList<Cat> getAllCat() {
        return new AnimalService().getCats();
    }

    @GetMapping("/dogs")
    public ArrayList<Dog> getAllDog() {
        return new AnimalService().getDogs();
    }

    @GetMapping("/animals")
    public ArrayList<Animal> getAlls() {
        return new AnimalService().getAll();
    }
}
