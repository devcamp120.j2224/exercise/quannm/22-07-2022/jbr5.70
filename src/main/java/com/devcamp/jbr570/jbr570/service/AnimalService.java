package com.devcamp.jbr570.jbr570.service;

import java.util.ArrayList;

import com.devcamp.jbr570.jbr570.model.Animal;
import com.devcamp.jbr570.jbr570.model.Cat;
import com.devcamp.jbr570.jbr570.model.Dog;

public class AnimalService {
    Cat cat1 = new Cat("Bo sua");
    Cat cat2 = new Cat("Cho");
    Cat cat3 = new Cat("Chuot");

    Dog dog1 = new Dog("Nhan");
    Dog dog2 = new Dog("Meo");
    Dog dog3 = new Dog("Trâu");

    public ArrayList<Animal> getAll() {
        ArrayList<Animal> listAll = new ArrayList<>();
        listAll.add(cat1);
        listAll.add(cat2);
        listAll.add(cat3);
        listAll.add(dog1);
        listAll.add(dog2);
        listAll.add(dog3);
        return listAll;
    }

    public ArrayList<Cat> getCats() {
        ArrayList<Cat> listCat = new ArrayList<>();
        listCat.add(cat1);
        listCat.add(cat2);
        listCat.add(cat3);
        return listCat;
    }

    public ArrayList<Dog> getDogs() {
        ArrayList<Dog> listDog = new ArrayList<>();
        listDog.add(dog1);
        listDog.add(dog2);
        listDog.add(dog3);
        return listDog;
    }
}
