package com.devcamp.jbr570.jbr570;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr570Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr570Application.class, args);
	}

}
