package com.devcamp.jbr570.jbr570.model;

public class Dog extends Mammal {

    public Dog(String name) {
        super(name);
        //TODO Auto-generated constructor stub
    }
    
    public void greets() {
        System.out.println("Woof");
    }

    public void greets(Dog another) {
        System.out.println("Wooof");
    }

    @Override
    public String toString() {
        return "Dog[Mammal[Animal[name=" + super.getName() + "]]]";
    }
}
