package com.devcamp.jbr570.jbr570.model;

public class Cat extends Mammal {

    public Cat(String name) {
        super(name);
        //TODO Auto-generated constructor stub
    }
    
    public void greets() {
        System.out.println("Meow");
    }

    @Override
    public String toString() {
        return "Cat[Mammal[Animal[name=" + super.getName() + "]]]";
    }
}
