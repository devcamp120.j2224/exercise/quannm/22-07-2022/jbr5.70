package com.devcamp.jbr570.jbr570.model;

public class Mammal extends Animal {

    public Mammal(String name) {
        super(name);
        //TODO Auto-generated constructor stub
    }
    
    @Override
    public String toString() {
        return "Mammal[Animal[name=" + super.getName() + "]]";
    }
}
